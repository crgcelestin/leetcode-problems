def bracket_match(text):
    stack_size = 0
    miss_match = 0
    for char in text:
        if char == "(":
            stack_size += 1
        elif char == ")" and stack_size > 0:
            stack_size -= 1
        else:
            miss_match += 1
    return stack_size + miss_match
