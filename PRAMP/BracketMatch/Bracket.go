//in golang
package main
import "fmt"

/*
“((” -> 2 ), )
")(" - ( , ) -> 2
"))" -> 2
"())(" -> '(' ())( ')' -> 2

# stack

 push opening char to the stack

 if we find a closing char remove from the stack
  ((
  stack = (,( -> 2

  (())
  stack = -> 0

  ())(

  stack = (
  res = 1  -> 2
  return res + len(stack)


*/
/*
 n = len(text)
 time = O(n)
 space =O(n)
 space  (((
*/

/*

*/
func BracketMatch(text string) int {
  stack := []rune{}
  var res int
  for _, c := range text {
    if c == '('{
      stack = append(stack, c)
    } else if c == ')' && len(stack) > 0{
      stack = stack[:len(stack)-1]
    }else{
      res++
    }

  }

  return res + len(stack)
}

func main() {
  fmt.Println(BracketMatch("(()")) // 1
  fmt.Println(BracketMatch("(())")) // 0
  fmt.Println(BracketMatch("())(")) // 2
}
