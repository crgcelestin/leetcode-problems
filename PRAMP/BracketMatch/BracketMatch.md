# March 4th, 2023

## __Bracket Match__
### Problem
- A string of brackets is considered correctly matched if every opening bracket in the string can be paired up with a later closing bracket, and vice versa. For instance, `“(())()”` is correctly matched, whereas `“)(“` and `“((”` aren’t. For instance, `“((”` could become correctly matched by adding two closing brackets at the end, so you’d return `2`.

- __Given a string that consists of brackets, write a function `bracketMatch` that takes a bracket string as an input and returns the minimum number of brackets you’d need to add to the input in order to make it correctly matched.__

- Explain the correctness of your code, and analyze its time and space complexities.

    ### __Examples__
    ```python
    input:  text = “(()”
    output: 1

    input:  text = “(())”
    output: 0

    input:  text = “())(”
    output: 2
    ```
## [Bracket 1 Solution](Bracket1.py)


## __Bracket Match 2__
### Problem
- Same Premise as the 1st Bracket Match
    - A string of brackets is considered correctly matched if every opening bracket in the string can be paired up with a later closing bracket, and vice versa. For instance, `“(())()”` is correctly matched, whereas `“)(“` and `“((”` aren’t. For instance, `“((”` could become correctly matched by adding two closing brackets at the end, so you’d return `2`.


- Central Problem: __Return either ‘balanced’ or ‘unbalanced’ depending on if a string with brackets has all open and closed brackets in sequence and are equal in occurrence__

    ### __Examples__
    ```python
    input: '{[()'
    output: 'unbalanced'

    input: '{}()[]'
    output: 'balanced'

    input: “())(”
    output: 'unbalanced'
    ```

## [Bracket 2 Solution](Bracket2.py)
