/*
Given a sorted array arr of distinct integers, write a function indexEqualsValueSearch that returns the lowest index i for which arr[i] == i. Return -1 if there is no such index. Analyze the time and space complexities of your solution and explain its correctness.
*/
/**
 * @param {string[]} arr
 * @returns {number}
 */
var AIE = function (arr) {
    let target = -1
    if (!arr.length) return target;
    let [left, right] = [0, arr.length - 1]
    while (left <= right) {
        let mid = Math.floor((left + right) / 2)
        if (arr[mid] === mid) {
            console.log(`this is ${mid}`)
            target = mid
            right = mid - 1
            console.log([target, right])
        }
        if (arr[mid] < mid) left = mid + 1;
        if (arr[mid] > mid) right = mid - 1;
    }
    return target
}


console.log(
    AIE(
        [0, -8, 2, 5, 4]
    )
)
