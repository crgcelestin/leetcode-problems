/**
 * @param {list[number]} nums
 * @returns {list[number]}
 */
var AAP = function (nums) {
    let res = Array(nums.length).fill(1)
    if (nums.length === 0 || nums.length == 1) {
        return []
    }
    let prefix = 1;
    for (let i = 0; i < nums.length; i++) {
        res[i] = prefix
        prefix *= nums[i]
    }
    let postfix = 1;
    for (let j = nums.length - 1; j > -1; j--) {
        res[j] *= postfix
        postfix *= nums[j]
    }
    return res
}

console.log(
    AAP(
        [8, 10, 2]
    )
)
